/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.rattanalak.exhaustivesearch;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Rattanalak
 */
public class ExhaustiveSearch {

    private int[] x;
    static int indexArray = 0;
    static ArrayList<ArrayList<Integer>> first = new ArrayList<>();
    static ArrayList<ArrayList<Integer>> last = new ArrayList<>();
    int sum1 = 0;
    int sum2 = 0;

    public ExhaustiveSearch(int[] x) {
        this.x = x;
    }

      

    static void makeArray(int arr[], int n, int r) {
        int data[] = new int[r];
        combine(arr, data, 0, n - 1, 0, r);
    }

    static void combine(int arr[], int data[], int start,
            int end, int index, int r) {
        ArrayList<Integer> temp = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                temp.add(data[j]);
            }
            first.add(indexArray, temp);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            combine(arr, data, i + 1, end, index + 1, r);
        }
    }
     public void processingEXS() {
        ArrayList<Integer> drop1 = new ArrayList<>();
        ArrayList<Integer> drop2 = new ArrayList<>();

        for (int o = 0; o < x.length; o++) {
            drop1.add(x[o]);
        }
        for (int i = 0; i < x.length; i++) {
            drop2.add(x[i]);
        }

        int arr[] = x;
        int n = arr.length;

        for (int i = x.length; i >= 0; i--) {
            makeArray(arr, n, i);
        }

        indexArray = 1;
        last.add(0, drop2);
        for (int j = 1; j < first.size(); j++) {
            drop1 = new ArrayList<>();
            for (int i = 0; i < x.length; i++) {
                drop1.add(x[i]);
            }
            for (int k = 0; k < first.get(j).size(); k++) {
                for (int u = 0; u < drop1.size(); u++) {
                    if (drop1.get(u) == first.get(j).get(k)) {
                        drop1.remove(u);
                        u--;
                    }
                }
            }
            last.add(indexArray, drop1);
            indexArray++;
        }
    }

  
    
     public void sum() {
        for (int i = 0; i < first.size(); i++) {
            if (first.contains(last.get(i))) {
                first.remove(i);
                last.remove(i);
            }
        }
        for (int i = 0; i < first.size(); i++) {

            for (int j = 0; j < first.get(i).size(); j++) {
                sum1 += first.get(i).get(j);
            }
            for (int j = 0; j < last.get(i).size(); j++) {
                sum2 += last.get(i).get(j);
            }
            if (sum1 == sum2) {
                System.out.println(first.get(i) + " " + last.get(i));
            }
        }
    }

}
